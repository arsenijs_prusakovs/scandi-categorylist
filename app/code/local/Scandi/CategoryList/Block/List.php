<?php
class Scandi_CategoryList_Block_List extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        $this->setTemplate('scandi/categorylist/list.phtml');
    }
    
    public function getCategoryId()
    {
        return $this->getData('category_id');
    }
}